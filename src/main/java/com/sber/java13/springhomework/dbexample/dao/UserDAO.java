package com.sber.java13.springhomework.dbexample.dao;

import com.sber.java13.springhomework.dbexample.model.User;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;

@Component
@Scope("prototype")
public class UserDAO {
    private final Connection connection;
    private final BookDAO bookDAO;
    public UserDAO(Connection connection, BookDAO bookDAO) {
        this.connection = connection;
        this.bookDAO = bookDAO; //инжектим букДАО через конструктор
    }
    
    public void addUser(User user) throws SQLException {
        PreparedStatement addQuery = connection.prepareStatement("insert into users" +
                "(surname, name, birthday, phone_number, email, titles_of_borrowed_books)" +
                "values (?, ?, ?, ?, ?, ?)");
        addQuery.setString(1, user.getUserSurname());
        addQuery.setString(2, user.getUserName());
        addQuery.setDate(3, Date.valueOf(user.getUserBirthday()));
        addQuery.setString(4, user.getUserPhoneNumber());
        addQuery.setString(5, user.getUserEmail());
        addQuery.setString(6, user.getUserTitlesOfBorrowedBooks());
        addQuery.executeUpdate();
    }
    
    public void getUserBooksInfo(String phoneNumber) throws SQLException {
        PreparedStatement selectQuery = connection.prepareStatement("select titles_of_borrowed_books from users" +
                " where phone_number = ?");
        selectQuery.setString(1, phoneNumber);
        ResultSet resultSet = selectQuery.executeQuery();
        while (resultSet.next()) {
            String result = resultSet.getString(1);
            String[] titles = result.split(", ");
            for (String e : titles) {
                System.out.println(bookDAO.getBookByTitle(e));
            }
        }
    }
}
