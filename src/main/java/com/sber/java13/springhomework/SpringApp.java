package com.sber.java13.springhomework;

import com.sber.java13.springhomework.dbexample.dao.UserDAO;
import com.sber.java13.springhomework.dbexample.model.User;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;

@SpringBootApplication
public class SpringApp implements CommandLineRunner {
    private final UserDAO userDAO;
    
    public SpringApp(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
    
    public static void main(String[] args) {
        SpringApplication.run(SpringApp.class, args);
    }
    
    @Override
    public void run(String... args) throws Exception {
        userDAO.addUser(new User(1, "Иванов", "Иван",
                LocalDate.of(2000, 12, 11),
                "+7-901-234-56-78", "farfra@mail.ru",
                "Недоросль, Доктор Живаго"));
        userDAO.addUser(new User(2, "Петр", "Петров",
                LocalDate.of(1998, 1, 15),
                "+7-922-00-12-89", "golang@gmail.com",
                "Сестра моя - жизнь, Путешествие из Петербурга в Москву"));
        
        userDAO.getUserBooksInfo("+7-901-234-56-78");
    }
}